package main

import (
	"reflect"
	"testing"
)

type TestCase struct {
	Describe  string
	Listeners []EventHandler
	Given     Events
	When      Command
	Expect    Events
	Error     error
}

type TestCases []*TestCase

func (testCase *TestCase) Run(t *testing.T) {
	target := testCase.When.Target()
	if given := testCase.Given; given != nil {
		if listeners := testCase.Listeners; listeners != nil {
			for _, l := range listeners {
				given.Replay(l)
			}
		}

		given.Replay(target)
	}

	events, err := target.HandleCommand(testCase.When)
	if !reflect.DeepEqual(events, testCase.Expect) {
		t.Fatalf("# %s\nExpected %v to equal %v", testCase.Describe, events, testCase.Expect)
	}

	if err != testCase.Error {
		t.Fatalf("# %s\nExpected error %v, got %v", testCase.Describe, testCase.Error, err)
	}
}

func (suite TestCases) Run(t *testing.T) {
	for _, testcase := range suite {
		testcase.Run(t)
	}
}
