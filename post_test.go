package main

import (
	"testing"
	"time"
)

func Test_Post_Publish(t *testing.T) {
	now := time.Now()
	TestCases{
		{
			Describe: "Publishing succeeds",
			When: &PublishPostCommand{
				Title:    " title ",
				Body:     " body ",
				Now:      now,
				AllPosts: PostMapRepository{},
			},
			Expect: Events{{
				"_name":        "post.published",
				"id":           "title",
				"body":         "body",
				"title":        "title",
				"published_at": now,
			}},
		}, {
			Describe: "Publishing failes if title is not unique",
			When: &PublishPostCommand{
				Title:    "title",
				Body:     "body",
				Now:      now,
				AllPosts: PostMapRepository{"title": true},
			},
			Error: ErrPostTitleExists,
		}, {
			Describe: "Publishing fails if title is empty",
			When: &PublishPostCommand{
				Title:    "",
				Body:     "body",
				Now:      now,
				AllPosts: PostMapRepository{},
			},
			Error: ErrPostTitleEmpty,
		}, {
			Describe: "Publishing fails if body is empty",
			When: &PublishPostCommand{
				Title:    "title",
				Body:     "",
				Now:      now,
				AllPosts: PostMapRepository{},
			},
			Error: ErrPostBodyEmpty,
		},
	}.Run(t)
}
