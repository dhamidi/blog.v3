package main

import (
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"reflect"
	"strings"
	"time"
)

type Command interface {
	Target() Aggregate
}

type Event map[string]interface{}

func (e Event) Name() string {
	return e["_name"].(string)
}

func (e Event) Id() string {
	return e["id"].(string)
}

type Events []Event

type EventHandler interface {
	HandleEvent(event Event, replaying bool)
}

type CommandHandler interface {
	HandleCommand(command Command) (Events, error)
}

func (events Events) Apply(handler EventHandler) {
	for _, e := range events {
		handler.HandleEvent(e, false)
	}
}

func (events Events) Replay(handler EventHandler) {
	for _, e := range events {
		handler.HandleEvent(e, true)
	}
}

type Aggregate interface {
	Id() string
	EventHandler
	CommandHandler
}

var (
	ErrStreamNotFound = errors.New("EventStore: stream not found")
)

type EventStore interface {
	Replay(handler EventHandler) error
	Find(streamId string) (Events, error)
	Save(events Events) error
}

type EventStoreInMemory struct {
	all      Events
	byStream map[string][]int
}

func NewEventStoreInMemory() *EventStoreInMemory {
	return &EventStoreInMemory{
		all:      Events{},
		byStream: map[string][]int{},
	}
}

func (store *EventStoreInMemory) Replay(handler EventHandler) error {
	store.all.Replay(handler)
	return nil
}

func (store *EventStoreInMemory) Find(streamId string) (Events, error) {
	offsets, found := store.byStream[streamId]
	if !found {
		return nil, ErrStreamNotFound
	}

	result := Events{}
	for _, index := range offsets {
		result = append(result, store.all[index])
	}

	return result, nil
}

func (store *EventStoreInMemory) Save(events Events) error {
	n := len(store.all)
	store.all = append(store.all, events...)

	for i, event := range events {
		store.byStream[event.Id()] = append(store.byStream[event.Id()], n+i)
	}

	return nil
}

type ApplicationService struct {
	Store EventStore

	allPosts    PostMapRepository
	allSessions SessionMapRepository
	listeners   []EventHandler
}

func (app *ApplicationService) Start() error {
	app.allPosts = PostMapRepository{}
	app.allSessions = SessionMapRepository{}
	app.listeners = []EventHandler{
		app.allPosts,
		app.allSessions,
	}

	return app.Store.Replay(app)
}

func (app *ApplicationService) HandleCommand(command Command) (Events, error) {
	target := command.Target()

	if history, err := app.Store.Find(target.Id()); err != nil {
		if err != ErrStreamNotFound {
			return nil, err
		}
	} else {
		history.Replay(target)
	}

	events, err := target.HandleCommand(command)
	if err != nil {
		return nil, err
	}

	if err := app.Store.Save(events); err != nil {
		return nil, err
	}

	events.Apply(app)

	return events, nil
}

func (app *ApplicationService) HandleEvent(event Event, replaying bool) {
	for _, l := range app.listeners {
		l.HandleEvent(event, replaying)
	}
}

func (app *ApplicationService) respondForCommand(w http.ResponseWriter, command Command) {
	log.Printf("command=%#v\n", command)
	if events, err := app.HandleCommand(command); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		enc := json.NewEncoder(w)
		enc.Encode(events)
	}
}

func (app *ApplicationService) requireLoggedInUser(w http.ResponseWriter, req *http.Request) bool {
	sessionId := req.Header.Get("X-Session-Id")
	if sessionId == "" {
		sessionId = req.FormValue("session_id")
	}

	if app.allSessions.Find(sessionId) {
		return true
	}

	http.Error(w, "Login required", http.StatusForbidden)

	return false
}

func (app *ApplicationService) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	route := fmt.Sprintf("%s %s", req.Method, req.URL.Path)
	switch route {
	case "POST /sessions":
		command := &LogInUserCommand{
			Username:    req.FormValue("username"),
			Password:    req.FormValue("password"),
			SessionId:   req.FormValue("session_id"),
			AllSessions: app.allSessions,
			Now:         time.Now(),
		}
		app.respondForCommand(w, command)
	case "POST /users":
		command := &SignUpUserCommand{
			Username: req.FormValue("username"),
			Password: req.FormValue("password"),
			Email:    req.FormValue("email"),
			Now:      time.Now(),
		}
		app.respondForCommand(w, command)
	case "POST /posts":
		if !app.requireLoggedInUser(w, req) {
			return
		}

		command := &PublishPostCommand{
			Title:    req.FormValue("title"),
			Body:     req.FormValue("body"),
			AllPosts: app.allPosts,
			Now:      time.Now(),
		}
		app.respondForCommand(w, command)
	default:
		http.Error(w, "Not found", http.StatusNotFound)
	}
}

type AttributeError struct {
	key string
	msg string
}

func (err *AttributeError) Error() string {
	return fmt.Sprintf("%s: %s", err.key, err.msg)
}

type UnknownCommandError struct {
	receiver string
	command  Command
}

func (err *UnknownCommandError) Error() string {
	messageName := reflect.TypeOf(reflect.ValueOf(err.command).Elem().Interface()).Name()
	return fmt.Sprintf("%s: cannot handle command: %s", err.receiver, messageName)
}

type User struct {
	id       string
	signedUp bool
	password string
}

var (
	ErrUserAlreadySignedUp           = &AttributeError{"user.id", "not_unique"}
	ErrUserPasswordEmpty             = &AttributeError{"user.password", "empty"}
	ErrUserEmailInvalid              = &AttributeError{"user.email", "needs_to_match_at"}
	ErrUserAuthenticationFailure     = &AttributeError{"user", "authentication_failed"}
	ErrUserLoggedInAlready           = &AttributeError{"user", "logged_in_already"}
	ErrUserSessionIdGenerationFailed = &AttributeError{"user.session_id", "generation_failed"}
)

func (u *User) Id() string {
	return u.id
}

func (u *User) HandleCommand(command Command) (Events, error) {
	switch cmd := command.(type) {
	case *SignUpUserCommand:
		return u.SignUp(cmd)
	case *LogInUserCommand:
		return u.LogIn(cmd)
	}

	return nil, &UnknownCommandError{"user", command}
}

func (u *User) HandleEvent(event Event, replaying bool) {
	switch event.Name() {
	case "user.signed-up":
		u.signedUp = true
		u.password = event["password"].(string)
	}
}

type SignUpUserCommand struct {
	Username string
	Email    string
	Password string
	Now      time.Time
}

func (cmd *SignUpUserCommand) Target() Aggregate {
	return &User{
		id: strings.TrimSpace(cmd.Username),
	}
}

func (u *User) SignUp(params *SignUpUserCommand) (Events, error) {
	username := strings.TrimSpace(params.Username)
	email := strings.TrimSpace(params.Email)
	password := params.Password

	if u.signedUp {
		return nil, ErrUserAlreadySignedUp
	}

	if password == "" {
		return nil, ErrUserPasswordEmpty
	}

	if !strings.Contains(email, "@") {
		return nil, ErrUserEmailInvalid
	}

	return Events{{
		"_name":        "user.signed-up",
		"id":           username,
		"email":        email,
		"password":     params.Password,
		"signed_up_at": params.Now,
	}}, nil
}

type SessionRepository interface {
	Find(sessionId string) bool
}

type SessionMapRepository map[string]bool

func (repo SessionMapRepository) HandleEvent(event Event, replaying bool) {
	switch event.Name() {
	case "user.logged-in":
		sessionId := event["session_id"].(string)
		repo[sessionId] = true
	}
}

func (repo SessionMapRepository) Find(sessionId string) bool {
	return repo[sessionId]
}

type LogInUserCommand struct {
	Username    string
	Password    string
	SessionId   string
	Now         time.Time
	AllSessions SessionRepository
}

func (cmd *LogInUserCommand) Target() Aggregate {
	return &User{
		id: cmd.Username,
	}
}

func (u *User) LogIn(params *LogInUserCommand) (Events, error) {
	username := strings.TrimSpace(params.Username)
	password := strings.TrimSpace(params.Password)
	sessionId := params.SessionId

	if sessionId == "" {
		key := make([]byte, 8)
		if _, err := rand.Read(key); err != nil {
			return nil, ErrUserSessionIdGenerationFailed
		}
		sessionId = hex.EncodeToString(key)
	}

	if params.AllSessions.Find(params.SessionId) {
		return nil, ErrUserLoggedInAlready
	}

	if !u.correctPassword(password) {
		return nil, ErrUserAuthenticationFailure
	}

	return Events{{
		"_name":        "user.logged-in",
		"id":           username,
		"session_id":   sessionId,
		"logged_in_at": params.Now,
	}}, nil
}

func (u *User) correctPassword(password string) bool {
	return u.password == password
}

type PostRepository interface {
	TitleExists(title string) bool
}

type PostMapRepository map[string]bool

func (repo PostMapRepository) HandleEvent(event Event, replaying bool) {
	switch event.Name() {
	case "post.published":
		title := event["title"].(string)
		repo[title] = true
	}
}

func (repo PostMapRepository) TitleExists(title string) bool {
	return repo[title]
}

type Post struct {
	id string
}

var (
	ErrPostTitleEmpty  = &AttributeError{"post.title", "empty"}
	ErrPostBodyEmpty   = &AttributeError{"post.body", "empty"}
	ErrPostTitleExists = &AttributeError{"post.title", "not_unique"}
)

func (p *Post) Id() string { return p.id }

func (p *Post) HandleCommand(command Command) (Events, error) {
	switch cmd := command.(type) {
	case *PublishPostCommand:
		return p.Publish(cmd)
	}

	return nil, &UnknownCommandError{"post", command}
}

func (p *Post) HandleEvent(event Event, replaying bool) {
	switch event.Name() {
	case "post.published":
		p.id = event["id"].(string)
	}
}

type PublishPostCommand struct {
	Now      time.Time
	Title    string
	Body     string
	AllPosts PostRepository
}

func (cmd *PublishPostCommand) Target() Aggregate {
	return &Post{
		id: cmd.Title,
	}
}

func (p *Post) Publish(params *PublishPostCommand) (Events, error) {
	title := strings.TrimSpace(params.Title)
	body := strings.TrimSpace(params.Body)

	if title == "" {
		return nil, ErrPostTitleEmpty
	}

	if body == "" {
		return nil, ErrPostBodyEmpty
	}

	if params.AllPosts.TitleExists(params.Title) {
		return nil, ErrPostTitleExists
	}

	return Events{
		Event{
			"_name":        "post.published",
			"title":        title,
			"body":         body,
			"published_at": params.Now,
			"id":           title,
		},
	}, nil
}

func main() {
	app := &ApplicationService{
		Store: NewEventStoreInMemory(),
	}
	app.Start()
	log.Fatal(http.ListenAndServe(os.Getenv("BLOG_LISTEN"), app))
}
