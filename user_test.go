package main

import (
	"testing"
	"time"
)

func Test_User_Signup(t *testing.T) {
	now := time.Now()
	TestCases{
		{
			Describe: "Signing up succeeds",
			When: &SignUpUserCommand{
				Username: "  my-user-name ",
				Email:    "user@localhost",
				Password: "password",
				Now:      now,
			},
			Expect: Events{{
				"_name":        "user.signed-up",
				"id":           "my-user-name",
				"email":        "user@localhost",
				"password":     "password",
				"signed_up_at": now,
			}},
		}, {
			Describe: "Email address needs to contain '@'",
			When: &SignUpUserCommand{
				Username: "my-user-name",
				Email:    "userlocalhost",
				Password: "password",
				Now:      now,
			},
			Error: ErrUserEmailInvalid,
		}, {
			Describe: "Password must not be empty",
			When: &SignUpUserCommand{
				Username: "my-user-name",
				Email:    "user@localhost",
				Password: "",
				Now:      now,
			},
			Error: ErrUserPasswordEmpty,
		}, {
			Describe: "Cannot sign-up twice for the same username",
			Given: Events{{
				"_name":        "user.signed-up",
				"id":           "my-user-name",
				"email":        "user@localhost",
				"password":     "password",
				"signed_up_at": now,
			}},
			When: &SignUpUserCommand{
				Username: "my-user-name",
				Email:    "user@localhost",
				Password: "password",
				Now:      now,
			},
			Error: ErrUserAlreadySignedUp,
		},
	}.Run(t)

}
